<?php

namespace Gsdk\Filemanager;

use Gsdk\Filemanager\Services\StorageService;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class FilemanagerServiceProvider extends ServiceProvider
{
    protected string $storage = 'public';

    protected ?string $routeDomain = null;

    protected string $routePrefix = 'filemanager';

    protected array $routeMiddleware = ['web'];

    public function boot(): void
    {
        StorageService::useStorage($this->storage);

        $this->registerRoutes();
    }

    protected function registerRoutes(): void
    {
        Route::group([
            'domain' => $this->routeDomain,
            'prefix' => $this->routePrefix,
            'middleware' => $this->routeMiddleware,
        ], function () {
            $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        });
    }
}
