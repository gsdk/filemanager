<?php

namespace Gsdk\Filemanager\Http\Controllers;

use Gsdk\Filemanager\FilesystemException;
use Gsdk\Filemanager\Services\FileFilter;
use Gsdk\Filemanager\Services\StorageService;
use Gsdk\Filemanager\Services\PathReader;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class FilemanagerController extends Controller
{
    protected string $urlPath = '/filemanager/file';

    protected string $noPhotoPath = '/images/nophoto.webp';

    public function __construct(private readonly StorageService $storageService)
    {
    }

    public function index(Request $request)
    {
        $html = realpath(__DIR__ . '/../../../dist/index.html');

        return (string)file_get_contents($html);
    }

    public function files(Request $request)
    {
        $path = (string)$request->input('path');
        $page = (int)$request->input('page', 1);
        $step = (int)$request->input('step', 100);
        $offset = ($page - 1) * $step;
        $term = $request->input('term');
        $fileType = $request->input('fileType');

        $tempFiles = (new PathReader($this->storageService))->read($path);
        $tempFiles = (new FileFilter($term, $fileType))->filter($tempFiles);

        $count = count($tempFiles);
        $files = [];
        $l = min($count, $offset + $step);
        for ($i = $offset; $i < $l; $i++) {
            $files[] = $tempFiles[$i];
        }

        return [
            'urlPath' => $this->urlPath . ($path ? '/' . $path : ''),
            'loadedPath' => $path,
            'step' => $step,
            'files' => $files,
            'count' => $count
        ];
    }

    public function upload(Request $request)
    {
        $path = (string)$request->input('path');

        $this->storageService->cd($path);

        $upload = $request->file('file');
        foreach ($upload as $file) {
            $this->storageService->upload($file);
        }

        return ['uploaded' => 1];
    }

    public function folder(Request $request)
    {
        $path = (string)$request->input('path');
        $name = (string)$request->input('name');

        try {
            $this->storageService->cd($path)->mkdir($name);
        } catch (FilesystemException $e) {
            return $this->returnException($e);
        }

        return ['name' => $name];
    }

    public function rename(Request $request)
    {
        $path = (string)$request->input('path');
        $prevName = (string)$request->input('file');
        $newName = (string)$request->input('name');

        try {
            $this->storageService->cd($path)->rename($prevName, $newName);
        } catch (FilesystemException $e) {
            return $this->returnException($e);
        }

        return ['name' => $newName];
    }

    public function move(Request $request)
    {
        $path = (string)$request->input('path');
        $folder = (string)$request->input('folder') ?: '/';

        $this->storageService->cd($path);

        $files = $request->input('files');
        try {
            foreach ($files as $name) {
                $this->storageService->moveToPath($name, $folder);
            }
        } catch (FilesystemException $e) {
            return $this->returnException($e);
        }

        return ['success' => true];
    }

    public function delete(Request $request)
    {
        $path = (string)$request->input('path');

        $this->storageService->cd($path);

        $files = $request->input('files');

        foreach ($files as $name) {
            $this->storageService->unlink($name, true);
        }

        return ['success' => 1];
    }

    private function returnException(FilesystemException $e)
    {
        return [
            'error' => true,
            'message' => $e->getMessage()
        ];
    }
}
