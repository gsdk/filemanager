<?php

namespace Gsdk\Filemanager\Services;

class FileFilter
{
    private readonly ?string $searchTerm;
    private readonly ?string $fileType;

    public function __construct(
        ?string $searchTerm = null,
        ?string $fileType = null
    ) {
        $this->searchTerm = mb_strtolower($searchTerm, 'utf8');
        $this->fileType = $fileType;
    }

    public function filter(array $files): array
    {
        return array_values(
            array_filter($files, fn($f) => $this->testByTerm($f) && $this->testByType($f))
        );
    }

    private function testByTerm($file): bool
    {
        return empty($this->searchTerm) || str_contains(mb_strtolower($file->name, 'utf8'), $this->searchTerm);
    }

    private function testByType($file): bool
    {
        if (empty($this->fileType)) {
            return true;
        }

        return match ($this->fileType) {
            'folder' => $file->type === 'folder',
            'file' => $file->type === 'file',
            'image' => $file->type === 'file' && str_starts_with($file->mime_type, 'image/'),
            default => false,
        };
    }
}