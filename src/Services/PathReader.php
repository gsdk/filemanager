<?php

namespace Gsdk\Filemanager\Services;

use Illuminate\Support\Facades\File;

class PathReader
{
    public function __construct(
        private readonly StorageService $storageService
    ) {
    }

    public function read(string $relativePath): array
    {
        $absolutePath = $this->storageService->path($relativePath);
        $folders = [];
        $files = [];
        $handle = opendir($absolutePath);
        while (false !== ($entry = readdir($handle))) {
            $filename = $absolutePath . DIRECTORY_SEPARATOR . $entry;
            if (in_array($entry, ['.', '..']) || "$relativePath/$entry" === '/.gitignore') {
                continue;
            }

            $file = new \stdClass();
            $file->name = $entry;
            $file->path = $relativePath;
            if (is_dir($filename)) {
                $file->type = 'folder';
                $folders[] = $file;
            } else {
                $file->type = 'file';
                $file->url = $this->storageService->url(
                    ($relativePath ? $relativePath . DIRECTORY_SEPARATOR : '') . $entry
                );
                $file->size = File::size($filename);
                $file->mime_type = File::mimeType($filename);
                $files[] = $file;
            }
        }
        closedir($handle);

        $sort = fn($a, $b) => $a->name <=> $b->name;
        usort($folders, $sort);
        usort($files, $sort);

        return array_values(array_merge($folders, $files));
    }
}
