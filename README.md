# sdk-filemanager

## Usage

1. Register routes

```php
use Gsdk\Filemanager\RouteRegistrar;

RouteRegistrar::register();
```

2. Check address `/filemanager`