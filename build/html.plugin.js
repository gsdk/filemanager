import fs from 'fs'

const basePath = process.cwd()
const includes = [
  '/node_modules/jquery/dist/jquery.slim.min.js'
]

function pngToUrl(icon) {
  return 'data:image/png;base64,'
    + fs.readFileSync(icon, { encoding: 'base64' })
}

function fontToUrl(file) {
  return 'data:application/font-woff2;charset=utf-8;base64,'
    + fs.readFileSync(file, { encoding: 'base64' })
}

function replace(subject, regexp, fn) {
  return subject.replace(regexp, (m, n) => {
    const f = basePath + '/resources' + n
    if (fs.existsSync(f)) {
      return fn(f)
    } else {
      console.warn(m + ' not exists!')
      return m
    }
  })
}

async function makeCss(bundle) {
  let css = Buffer.from(bundle.source).toString()
  css = css.replace('@charset "UTF-8";', '')
  css = replace(css, /\.\.(\/icons\/.+?\.png)/g, (f) => pngToUrl(f))
  css = replace(css, /\.\.(\/fonts\/.+?\.woff2)/g, (f) => fontToUrl(f))

  return css
}

export default function () {
  return {
    name: 'html',
    async renderChunk(code, chunk, outputOptions) { },
    async generateBundle(options_, bundle) {
      const templateFile = basePath + '/resources/views/index.html'
      const distFile = basePath + '/dist/index.html'
      let html = fs.readFileSync(templateFile, "utf8");
      html = html.replace('${styles}', '<style>' + await makeCss(bundle['index.css']) + '</style>')
      html = html.replace('${scripts}', '<script>'
        + includes.map(n => fs.readFileSync(basePath + n, "utf8")).join('')
        + bundle['index.js'].code
        + '</script>')

      fs.writeFileSync(distFile, html, 'utf-8')
    }
  };
}