import {optimize} from "svgo"

function encodeStr(svgStr) {
  const encoded = encodeURIComponent(svgStr)
    .replace(/%20/g, " ")
    .replace(/%3D/g, "=")
    .replace(/%3B/g, ";")
    .replace(/%3A/g, ":")
    .replace(/%2F/g, "/")
    .replace(/%2C/g, ",")
    .replace(/%22/g, "'");

  return `data:image/svg+xml,${encoded}`;
}

export default function svgToUrl(svg) {
  const data = fs.readFileSync(svg)
  const { plugins = [], ...rest } = {}
  const s = optimize(data.toString(), {
    ...rest,
    plugins: [
      ...(plugins.length > 0 ? plugins : ["preset-default"]),
      "removeXMLNS",
    ],
  }).data.replace(/^<svg/g, `<svg xmlns="http://www.w3.org/2000/svg"`)

  return encodeStr(s)
}