import postcss from 'rollup-plugin-postcss'
import autoprefixer from 'autoprefixer'
import terser from '@rollup/plugin-terser'
import commonjs from '@rollup/plugin-commonjs'
import html from "./build/html.plugin.js"
import resolve from '@rollup/plugin-node-resolve';

// const pkg = require('./package.json');
const ENV_PROD = process.env.BUILD === 'production';
// ['node_modules/jquery/dist/jquery.min.js',
export default {
  // external: ['jquery'],
  input: 'resources/js/index.js',
  output: {
    file: `dist/index.js`,
    format: 'iife',
    sourcemap: false,
    // extend: true,
    globals: {
      // jquery: '$'
    },
    plugins: [
      ENV_PROD && terser({
        output: {
          comments: () => {},
        },
      }),
    ]
  },
  plugins: [
    commonjs({
      sourceMap: false
    }),
    resolve(),
    // inject({
    //   $: 'jquery',
    //   exclude: '**/*.scss'
    // }),
    postcss({
      extract: 'index.css',
      plugins: [autoprefixer],
      minimize: ENV_PROD,
    }),
    html()
  ]
}