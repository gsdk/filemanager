<?php

use Gsdk\Filemanager\Http\Controllers\FilemanagerController;
use Illuminate\Support\Facades\Route;

Route::controller(FilemanagerController::class)
    ->group(function () {
        Route::get('/', 'index')->name('filemanager.index');
        Route::get('/files', 'files');
        Route::post('/upload', 'upload');
        Route::post('/move', 'move');
        Route::post('/folder', 'folder');
        Route::post('/rename', 'rename');
        Route::post('/delete', 'delete');
    });
