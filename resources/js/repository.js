async function post(method, data) {
  const r = await fetch(`/filemanager/${method}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(data)
  })
  const result = await r.json()
  if (result.error) {
    alert(result.message)

    throw new Error(result.message)
  }
  return result;
}

export async function getFiles(params) {
  params.r = Math.random();
  const r = await fetch('/filemanager/files?' + new URLSearchParams(params))

  return await r.json();
}

export async function createFolder(data) {
  return await post('folder', data)
}

export async function remove(data) {
  return await post('delete', data)
}

export async function rename(data) {
  return await post('rename', data)
}

export async function move(data) {
  return await post('move', data)
}