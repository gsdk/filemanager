// import jQuery from 'jquery'
//
// window.$ = window.jQuery = jQuery;

import 'jquery-ui'
//@deprecated
import "jquery-ui/ui/version"
import "jquery-ui/ui/data"
import "jquery-ui/ui/disable-selection"
import "jquery-ui/ui/focusable"
import "jquery-ui/ui/form"
import "jquery-ui/ui/ie"
import "jquery-ui/ui/keycode"
import "jquery-ui/ui/labels"
import "jquery-ui/ui/jquery-patch"
import "jquery-ui/ui/plugin"
import "jquery-ui/ui/safe-active-element"
import "jquery-ui/ui/safe-blur"
import "jquery-ui/ui/scroll-parent"
import "jquery-ui/ui/tabbable"
import "jquery-ui/ui/unique-id"
//@end deprecated
import 'jquery-ui/ui/widgets/mouse'
import 'jquery-ui/ui/widgets/draggable'
import 'jquery-ui/ui/widgets/droppable'

import "../scss/index.scss"

import FileManager from "./manager"

$(() => {
  const fileManager = new FileManager(document.body, {});
  const url_string = window.location.href;
  const url = new URL(url_string);
  const type = url.searchParams.get("type");
  if (type)
    fileManager.addressbar.setFilter(type);
  fileManager.setView(url.searchParams.get("view") || 'grid');

  const owner = window.opener || window.parent || window.top;
  fileManager.bind('choose', function (file) {
    owner.postMessage({
      sender: 'filemanager',
      mimeType: file.mimeType,
      name: file.name,
      url: file.src
    }, "*");
  });
})
